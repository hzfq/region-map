package hzfq.region;

import java.io.File;

import hzfq.region.util.RegionParser;

/**
 * Created by huzhifengqing@gmail.com since 2018年8月29日 下午7:34:39.
 */
public class Application {

	public static void main(String[] args) throws Exception {
		String root = System.getProperty("user.dir");
		RegionParser.parseRegion(new File(root + "/src/main/resources/province.txt"));
	}
}
