package hzfq.region.domain;

/**
 * Created by huzhifengqing@gmail.com since 2018年8月29日 下午10:39:10.
 */
public class Region {

	private Integer code;
	private String region;

	public Region() {
	}

	public Region(Integer code, String region) {
		this.code = code;
		this.region = region;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "Region [code=" + code + ", region=" + region + "]";
	}

}
