package hzfq.region.domain;

import java.util.List;

/**
 * Created by huzhifengqing@gmail.com since 2018年8月29日 下午10:14:55.
 */
public class Province {

	private Integer code;
	private String province;
	private List<City> cities;

	public Province() {
	}

	public Province(Integer code, String province, List<City> cities) {
		this.code = code;
		this.province = province;
		this.cities = cities;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return "Province [code=" + code + ", province=" + province + ", cities=" + cities + "]";
	}

}
