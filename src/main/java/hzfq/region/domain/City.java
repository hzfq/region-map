package hzfq.region.domain;

import java.util.List;

/**
 * Created by huzhifengqing@gmail.com since 2018年8月29日 下午10:16:32.
 */
public class City {

	private Integer code;
	private String city;
	private List<Region> regions;

	public City() {
	}

	public City(Integer code, String city, List<Region> regions) {
		this.code = code;
		this.city = city;
		this.regions = regions;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	@Override
	public String toString() {
		return "City [code=" + code + ", city=" + city + ", regions=" + regions + "]";
	}

}
